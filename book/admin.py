from django.contrib import admin
from django.db import models
from .models import *
# Register your models here.


class BookAdmin(admin.ModelAdmin):
    # fields = ('book_name','author','published_date','number_of_books')
    list_display = (
        '__str__','book_name','author','published_date','number_of_books'
    )
    list_filter = ('book_name','author','published_date')

admin.site.register(Book,BookAdmin)
