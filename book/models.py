from django.db import models


# Create your models here.



class Book(models.Model):
    """
    Book model

    """
    book_name = models.CharField(max_length=230, default=None, unique=True, db_index=True)
    author = models.CharField(max_length=100, default=None, db_index=True)
    published_date = models.DateField(verbose_name='published_date', default=None)
    number_of_books = models.IntegerField()
    rack_number = models.IntegerField()

    class Meta:
        verbose_name_plural = 'Books'
        verbose_name = 'Book'

    def __str__(self):
        return '{} author by {}'.format(self.book_name, self.author)

    def get_book(self, id):
        """get the book by id"""
        try:
            return Book.objects.get(id=id)
        except:
            return None
