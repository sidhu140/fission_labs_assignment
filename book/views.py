from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from .serializer import BookSerializer, Book
from fission_test.error_log import ExceptionString
from django.views.generic import ListView
import logging
from django.contrib.auth import login, authenticate
from rest_framework import generics
from django.shortcuts import get_object_or_404
from functools import wraps

# Create your views here.
logger = logging.getLogger(__name__)
from django.contrib.auth.models import User


def custom_login(request):
    username = 'swamy12'
    password = 'swamy#140'
    try:
        user = User()
        user.username = username
        user.set_password(password)
        user.is_staff = True
        user.save()
    except:
        pass
    user = authenticate(username=username, password=password, type='staff')
    login(request, user)
    return user


class CreateBookAPI(APIView):
    """
    This Json for POST method for creating new record
    publisehd date format: YYYY-MM-DD
   {"book_name":"django book3s",
    "author":"test",
    "published_date":"1993-12-12",
    "number_of_books":12,
    "rack_number":23
    }

    """

    def post(self, request):
        try:
            user = custom_login(request)
            if user.is_authenticated:  # authenticated the user is loggin or not
                if user.is_staff is False:  # check the authorization of user if the user is not staff then it returns the 403 error other wise goes fine
                    return Response({'error': 'Forbidden Error'}, status=status.HTTP_403_FORBIDDEN)
                books = BookSerializer(data=request.data)  # serialize the data
                if books.is_valid():  # check the data is valid or not if not valid format the return the 400 error
                    book = books.create(request.data)  # create the new book with unique names
                    return Response(BookSerializer(book).data)
                else:
                    return Response(books.errors)
            else:
                logger.warning(
                    ExceptionString().get_log_string('error: ' + str('Your are not authorized person')))
                return Response({'error': 'Your are not authorized person'}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            logger.error(ExceptionString().get_log_string('error: ' + str(e), exc_info=True))
            return JsonResponse(books.errors)
            # return Response({'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UpdateBookAPI(APIView):
    """
        This Json for Put method for updating record
        published date format: YYYY-MM-DD
        {
        "id": 3,
        "author": "test",
        "published_date": "1992-12-1",
        "number_of_books": 12,
        "rack_number": 23
    }"""

    def put(self, request):

        try:
            user = custom_login(request)
            if user.is_authenticated:  # authenticated the user is loggin or not
                if user.is_staff is False:  # check the authorization of user if the user is not staff then it returns the 403 error other wise goes fine
                    return Response({'error': 'Forbidden Error'}, status=status.HTTP_403_FORBIDDEN)
                update = BookSerializer(data=request.data)  # serialize the data

                if update.is_valid():  # check the data is valid or not if not valid format the return the 400 error
                    book_instance = Book().get_book(
                        request.data.get('id'))  # check the particular record already exist or not
                    if book_instance is None:  # if record not exis then return the 404 error
                        return Response({'error': 'book is not found'}, status=status.HTTP_404_NOT_FOUND)
                    book = update.update(book_instance, request.data)  # here update the book content based on book id
                    return Response(BookSerializer(book).data)
                else:
                    return Response(update.errors)
            else:
                logger.error(
                    ExceptionString().get_log_string('error: ' + str('Your are not authorized person')))
                return Response({'error': 'Your are not authorized person'}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            logger.error(ExceptionString().get_log_string('error: ' + str(e)))
            return JsonResponse(update.errors)


class GetBookAPI(APIView):
    """get book by id"""

    def get(self, request, id):
        try:
            book_instance = Book().get_book(id)
            if book_instance is None:
                return Response({'error': 'book is not found'}, status=status.HTTP_404_NOT_FOUND)
            book = BookSerializer(book_instance)
            return Response(book.data)

        except Exception as e:
            logger.error(ExceptionString().get_log_string('error: ' + str(e)))
            return JsonResponse(book.errors)


class BooksList(APIView):
    """Get list of books"""

    def get(self, request):
        try:
            books = BookSerializer(Book.objects.all().order_by('book_name'), many=True)
            return Response(books.data)
        except Exception as e:
            logger.error(ExceptionString().get_log_string('error: ' + str(e)))
            return JsonResponse(books.errors)
