"""fission_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views, api_view

urlpatterns = [

    path('create/', views.CreateBookAPI.as_view(), name='create_book'),
    path('update/', views.UpdateBookAPI.as_view(), name='update_book'),
    path('get_book/<int:id>/', views.GetBookAPI.as_view(), name='get_book'),
    path('all_books/', views.BooksList.as_view(), name='all_books'),

    #### oauth2 authentications
    path('login/', api_view.Login.as_view()),
    path('create-book/', api_view.CreateBookAPI.as_view()),
    path('update-book/', api_view.UpdateBookAPI.as_view()),

]
