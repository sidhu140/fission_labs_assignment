from rest_framework import serializers
from .models import Book


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'

    def create(self, validated_data):
        """this method is used to create the new book and return the book data
        :rtype: object
        """
        book = Book.objects.create(**validated_data)
        return book

    def update(self, instance, validated_data):
        """this method is used to update the already existing book based on the book instance
        :rtype: object
        """
        instance.author = validated_data.get('author')
        instance.published_date = validated_data.get('published_date')
        instance.number_of_books = validated_data.get('number_of_books')
        instance.rack_number = validated_data.get('rack_number')
        instance.save()
        return instance
