from django.test import TestCase
from .models import Book


class BookTestCase(TestCase):
    def setUp(self):
        Book.objects.create(book_name="lion", author='test1', published_date='1992-2-3', number_of_boos=10,
                            rack_number=123)
        Book.objects.create(book_name="jungle book", author='test1', published_date='1992-2-3', number_of_boos=10,
                            rack_number=124)
        Book.objects.create(book_name="Man hunter", author='test1', published_date='1992-2-3', number_of_boos=10,
                            rack_number=124)

    def retrieve_the_book(self):
        """"""
        lion = Book.objects.get(book_name="lion")
        jungle = Book.objects.get(book_name="jungle book")
        jungle_invalid = Book.objects.get(book_name="jungle booker")
        self.assertEqual(lion)
        self.assertEqual(jungle)
        self.assertEqual(jungle_invalid)


# Create your tests here For rest api based.
from book.models import *
import json
from rest_framework import status
from django.test import TestCase
import requests as Client
from django.urls import reverse
from django.conf import settings


class APIBookTest(TestCase):
    """ Testing Book API """
    path = settings.BASE_PATH + 'book/'

    def test_post_book(self):
        response = Client.post(self.path + 'create/', {"book_name": "pairets of djadfddsddddddddngo",
                                                       "author": "test",
                                                       "published_date": "1993-12-12",
                                                       "number_of_books": 20,
                                                       "rack_number": 120})
        if response.status_code == status.HTTP_201_CREATED:
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        elif response.status_code == status.HTTP_400_BAD_REQUEST:
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        elif response.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR:
            self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        elif response.status_code == status.HTTP_401_UNAUTHORIZED:
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        elif response.status_code == status.HTTP_403_FORBIDDEN:
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_book_bad_request(self):
        def test_post_book(self):
            response = Client.post(self.path + 'create/', {"book_name": "pairets of django2",
                                                           "author": "test",
                                                           "published_date": "12-12-1992",
                                                           "number_of_books": 20,
                                                           "rack_number": 120})
            if response.status_code == status.HTTP_201_CREATED:
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            if response.status_code == status.HTTP_400_BAD_REQUEST:
                self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            elif response.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR:
                self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
            elif response.status_code == status.HTTP_401_UNAUTHORIZED:
                self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
            elif response.status_code == status.HTTP_403_FORBIDDEN:
                self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_book(self):
        response = Client.post(self.path + 'update/', {"id": 3,
                                                       "author": "test",
                                                       "published_date": "1993-12-12",
                                                       "number_of_books": 20,
                                                       "rack_number": 120})
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        elif response.status_code == status.HTTP_400_BAD_REQUEST:
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        elif response.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR:
            self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        elif response.status_code == status.HTTP_401_UNAUTHORIZED:
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        elif response.status_code == status.HTTP_403_FORBIDDEN:
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    def test_update_book_not_found(self):
        def test_update_book(self):
            response = Client.post(self.path + 'update/', {"id": 339,
                                                           "author": "test",
                                                           "published_date": "1993-12-12",
                                                           "number_of_books": 20,
                                                           "rack_number": 120})
            if response.status_code == status.HTTP_404_NOT_FOUND:
                self.assertEqual(response.status_code, status.HTTP_200_OK)
            elif response.status_code == status.HTTP_400_BAD_REQUEST:
                self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            elif response.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR:
                self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
            elif response.status_code == status.HTTP_401_UNAUTHORIZED:
                self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
            elif response.status_code == status.HTTP_403_FORBIDDEN:
                self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_book_by_id(self):
        response = Client.get(self.path + 'get_book/2/', {})
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        elif response.status_code == status.HTTP_404_NOT_FOUND:
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_book_by_id_not_found(self):
        response = Client.get(self.path + 'get_book/200/', {})

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_all_books(self):
        response = Client.get(self.path + 'all_books/', {})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
