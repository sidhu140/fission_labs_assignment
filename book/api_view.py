from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from .serializer import BookSerializer, Book
from fission_test.error_log import ExceptionString
from django.views.generic import ListView
import logging
from django.contrib.auth import login, authenticate
import requests
from django.conf import settings

logger = logging.getLogger(__name__)
# Create your views here.
CLIENT_ID = 'LbdfKcVFGniaR6tzptfyxzmI9xTEuy13EBlLyYiX'
CLIENT_SECRET = 'jF7GrdY44OvDQwMpjzHMEHBeKU9MOSnSeBuaNPsxdp65GtHvBnDWyZQMHF2slZ0A5YafaMo6krwnYwo2dqS6YjYFfii9NwLGxm6DkyX0OZtR484musgU8XOk3KmevZqE'

from django.contrib.auth.models import User


class Login(APIView):
    """
     user is already exist with valid details then it login and return the token details other wise it create the new user to return the token details
    create user to the server. Input should be in the format:
               {"username": "username", "password": "1234abcd"}"""

    def post(self, request):
        """ Registers user to the server. Input should be in the format:
            {"username": "username", "password": "1234abcd","email":"example@mail.com"}"""
        try:

            username = request.data.get('username')
            password = request.data.get('password')
            try:
                user = User()
                user.username = username
                user.set_password(password)
                user.is_staff = True
                user.save()
            except:
                pass
            auth_user = authenticate(username=username, password=password, type='staff')
            if auth_user:
                login(request, auth_user)
                user_info = {
                    'grant_type': 'password',
                    'username': username,
                    'password': password,
                    'client_id': CLIENT_ID,
                    'client_secret': CLIENT_SECRET,
                }
                token_resp = requests.post(settings.BASE_PATH + "oauth2/token/", data=user_info)

                token_resp = token_resp.json()
                return Response(token_resp, status=status.HTTP_200_OK)


            else:
                return Response({"error": "Something went wrong"}, status=status.HTTP_401_UNAUTHORIZED)

        except Exception as e:
            return Response({"error": "Something went wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CreateBookAPI(APIView):
    """
    This Json for POST method for creating new record
    publisehd date format: YYYY-MM-DD
   {"book_name":"django book3s",
    "author":"test",
    "published_date":"1993-12-12",
    "number_of_books":12,
    "rack_number":23
    }

    """

    def post(self, request):
        try:
            if request.user.is_authenticated:  # authenticated the user is loggin or not
                if request.user.is_staff is False:  # check the authorization of user if the user is not staff then it returns the 403 error other wise goes fine
                    return Response({'error': 'Forbidden Error'}, status=status.HTTP_403_FORBIDDEN)
                books = BookSerializer(data=request.data)  # serialize the data
                if books.is_valid():  # check the data is valid or not if not valid format the return the 400 error
                    book = books.create(request.data)  # create the new book with unique names
                    return Response(BookSerializer(book).data)
                else:
                    return Response(books.errors)
            else:
                logger.warning(
                    ExceptionString().get_log_string('error: ' + str('Your are not authorized person')))
                return Response({'error': 'Your are not authorized person'}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            logger.error(ExceptionString().get_log_string('error: ' + str(e), exc_info=True))
            return JsonResponse(books.errors)
            # return Response({'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UpdateBookAPI(APIView):
    """
        This Json for Put method for updating record
        published date format: YYYY-MM-DD
        {
        "id": 3,
        "author": "test",
        "published_date": "1992-12-1",
        "number_of_books": 12,
        "rack_number": 23
    }"""

    def put(self, request):

        try:
            if request.user.is_authenticated:  # authenticated the user is loggin or not
                if request.user.is_staff is False:  # check the authorization of user if the user is not staff then it returns the 403 error other wise goes fine
                    return Response({'error': 'Forbidden Error'}, status=status.HTTP_403_FORBIDDEN)
                update = BookSerializer(data=request.data)  # serialize the data

                if update.is_valid():  # check the data is valid or not if not valid format the return the 400 error
                    book_instance = Book().get_book(
                        request.data.get('id'))  # check the particular record already exist or not
                    if book_instance is None:  # if record not exis then return the 404 error
                        return Response({'error': 'book is not found'}, status=status.HTTP_404_NOT_FOUND)
                    book = update.update(book_instance, request.data)  # here update the book content based on book id
                    return Response(BookSerializer(book).data)
                else:
                    return Response(update.errors)
            else:
                logger.error(
                    ExceptionString().get_log_string('error: ' + str('Your are not authorized person')))
                return Response({'error': 'Your are not authorized person'}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            logger.error(ExceptionString().get_log_string('error: ' + str(e)))
            return JsonResponse(update.errors)
