class ExceptionString:
    def get_log_string(self, message, request=None):
        log = message
        if request:
            user_status = ''
            if request.user.is_authenticated():
                user_status = 'User: ' + str(request.user.id)
            log = "{0} || Session: {1} || Path: {2} || {3}".format(user_status, self.get_session_key(request),
                                                                   request.path,
                                                                   message)
        return log

    def get_session_key(self, request):
        return request.session._session_key
